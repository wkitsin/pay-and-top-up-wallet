# Pay and Top Up Wallet

**Installation**

```
1. yarn 
2. cd ios && pod install
3. yarn ios

Run test 
1. yarn test
```

**Assumptions**

```
1. Only 2 users are on the system, alice and bob 
2. You can log in as alice and bob using their username
3. You can view alice's and bob's balance and owe amount as long as you are logged as any user
4. You can pay or topup as _alice_ if you are logged in as _alice_
```

- Navigation: React Native Navigation 
- State Management: React Redux and Redux Saga middleware (first project playing around with redux saga, limited usage on Effects. But it was fun)
- AsyncStorage to store session, refreshing the app does not lose the session

**Limitations**
```
1. The redux reducer does not work well with hot reload. saving the reducer and action files will reset the state
2. The application looks better on iOS as it's developed on an iOS simulator
```

Mini Demo as per the example
https://cln.sh/PVan4E


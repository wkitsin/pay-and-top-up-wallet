declare interface TypedAction<T, U> {
  type: T;
  payload: U;
}

declare interface TypedActionEmpty<T> {
  type: T;
}

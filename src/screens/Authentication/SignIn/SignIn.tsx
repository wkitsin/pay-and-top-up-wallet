import { ErrorMessage } from "@hookform/error-message";
import * as React from "react";
import { Controller, useForm } from "react-hook-form";
import { Button, Input } from "react-native-elements";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Container from "../../../components/Container/Container";
import { userSessionActionCreators } from "../../../redux/actions/userSession";
import { AuthenticationStackNavProps } from "../AuthenticationParamsList";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { users } from "../../../data/data";
import { Text } from "react-native";

const schema = yup.object().shape({
  name: yup
    .string()
    .oneOf(
      [users.alice, users.bob],
      "Your username must either be alice or bob to proceed"
    )
    .required("Your username must be required")
});
interface SignInDispatchProps {
  setUserSession: typeof userSessionActionCreators.startLoginUserSession;
}

type SignInProps = SignInDispatchProps & AuthenticationStackNavProps<"SignIn">;

const SignIn: React.FC<SignInProps> = ({ setUserSession, navigation }) => {
  const { errors, control, trigger, watch } = useForm({
    defaultValues: { name: "" },
    resolver: yupResolver(schema)
  });

  React.useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button
          title="Sign In"
          type="clear"
          style={{ marginHorizontal: 20 }}
          onPress={async () => {
            const result = await trigger();

            if (result) {
              setUserSession(watch("name"));
            }
          }}
        />
      )
    });
  }, []);

  return (
    <Container>
      <Controller
        control={control}
        render={({ onChange }) => (
          <Input
            autoCapitalize="none"
            placeholder="Enter your name"
            onChangeText={value => onChange(value)}
            label="Name"
            labelStyle={{ color: "black" }}
          />
        )}
        name="name"
        rules={{ required: true }}
        defaultValue=""
      />
      {errors.name && (
        <ErrorMessage
          name="name"
          errors={errors}
          render={({ message }) => (
            <Text style={{ color: "red", paddingHorizontal: 10 }}>
              {message}
            </Text>
          )}
        />
      )}
    </Container>
  );
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { setUserSession: userSessionActionCreators.startLoginUserSession },
    dispatch
  );

export default connect(null, mapDispatchToProps)(SignIn);

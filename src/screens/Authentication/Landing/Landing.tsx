import * as React from "react";
import { Button, Text } from "react-native-elements";

import Container from "../../../components/Container/Container";
import { AuthenticationStackNavProps } from "../AuthenticationParamsList";

const Landing: React.FC<AuthenticationStackNavProps<"Landing">> = ({
  navigation
}) => (
  <Container centered style={{ padding: 20 }}>
    <Text h1 style={{ marginBottom: 40, color: "black", textAlign: "center" }}>
      Welcome to the OCBC wallet!
    </Text>

    <Button
      title="Sign In"
      onPress={() => {
        navigation.navigate("SignIn");
      }}
    />
  </Container>
);

export default Landing;

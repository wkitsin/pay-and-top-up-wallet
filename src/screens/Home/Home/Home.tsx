import * as React from "react";
import { ListItem, Text, Button } from "react-native-elements";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Container from "../../../components/Container/Container";
import { transactionActionCreators } from "../../../redux/actions/transaction";
import { userSessionActionCreators } from "../../../redux/actions/userSession";
import { BalanceState } from "../../../redux/reducers/balance";
import { GlobalState } from "../../../redux/reducers/type";
import { UserSessionState } from "../../../redux/reducers/userSession";
import { HomeStackNavProps } from "../HomeParamsList";
import PayAndTopUpOverlay from "./components/PayAndTopUpOverlay";

interface HomeStateProps {
  userSession: UserSessionState;
  usersBalance: BalanceState[];
}

interface HomeDispatchProps {
  removeUserSession: typeof userSessionActionCreators.removeUserSession;
  startTransaction: typeof transactionActionCreators.start;
}

type HomeProps = HomeStateProps & HomeDispatchProps & HomeStackNavProps<"Home">;

const Home: React.FC<HomeProps> = ({
  removeUserSession,
  userSession,
  usersBalance,
  navigation,
  startTransaction
}) => {
  const [transactionType, setTransactionType] = React.useState<"pay" | "topUp">(
    "pay"
  );

  navigation.setOptions({
    headerRight: () => (
      <Button
        type="clear"
        title="Sign Out"
        onPress={() => {
          removeUserSession();
        }}
      />
    )
  });

  return (
    <Container style={{ padding: 10 }} noSafe>
      <Text h3>Welcome {userSession.username}! </Text>

      {usersBalance.map(({ username, balance, owe }, index) => (
        <ListItem key={index} bottomDivider style={{ width: "100%" }}>
          <ListItem.Content>
            <ListItem.Title>Username: {username}</ListItem.Title>
            <ListItem.Title>Balance: RM {balance}</ListItem.Title>
            <ListItem.Title>Owe: RM {owe?.amount}</ListItem.Title>
          </ListItem.Content>
        </ListItem>
      ))}

      <PayAndTopUpOverlay type={transactionType} />

      <Button
        title="Top Up"
        style={{ marginTop: 30 }}
        onPress={() => {
          setTransactionType("topUp");
          startTransaction();
        }}
      />
      <Button
        title="Pay"
        style={{ marginTop: 30 }}
        onPress={() => {
          setTransactionType("pay");
          startTransaction();
        }}
      />
    </Container>
  );
};

const mapStateToProps = (state: GlobalState) => ({
  userSession: state.userSession,
  usersBalance: state.balance
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      removeUserSession: userSessionActionCreators.removeUserSession,
      startTransaction: transactionActionCreators.start
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Home);

import React from "react";
import { Controller, useForm } from "react-hook-form";
import { Button, Input, Overlay, Text } from "react-native-elements";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { users } from "../../../../data/data";
import { balanceActionCreators } from "../../../../redux/actions/balance";
import { transactionActionCreators } from "../../../../redux/actions/transaction";
import { TransactionState } from "../../../../redux/reducers/transaction";
import { GlobalState } from "../../../../redux/reducers/type";
import { UserSessionState } from "../../../../redux/reducers/userSession";

interface PayAndTopUpOverlayProps {
  type: "pay" | "topUp";
}

interface PayAndTopUpOverlayStateProps {
  userSession: UserSessionState;
  transaction: TransactionState;
}

interface PayAndTopUpOverlayDispatchProps {
  topUp: typeof balanceActionCreators.topUp;
  pay: typeof balanceActionCreators.pay;
  endTransaction: typeof transactionActionCreators.end;
}

type Props = PayAndTopUpOverlayProps &
  PayAndTopUpOverlayStateProps &
  PayAndTopUpOverlayDispatchProps;

const PayAndTopUpOverlay: React.FC<Props> = ({
  pay,
  type,
  topUp,
  userSession,
  transaction,
  endTransaction
}) => {
  const { control, watch } = useForm({
    defaultValues: { amount: 0 }
  });

  const receiver =
    users.alice === userSession.username ? users.bob : users.alice;

  return (
    <Overlay
      overlayStyle={{ padding: 50 }}
      isVisible={transaction.startTransaction ?? false}
      onBackdropPress={() => endTransaction()}
    >
      <>
        <Text>
          How much do you want to {type} {type === "pay" ? receiver : null}?
        </Text>

        <Controller
          control={control}
          render={({ onChange }) => (
            <Input
              containerStyle={{ width: 200 }}
              keyboardType="number-pad"
              placeholder="Enter your amount"
              onChangeText={value => onChange(value)}
            />
          )}
          name="amount"
          rules={{ required: true }}
          defaultValue="0"
        />

        <Button
          title="Submit"
          type="outline"
          onPress={() => {
            if (type === "pay") {
              pay(watch("amount"), receiver, userSession.username!);
            } else {
              topUp(watch("amount"), userSession.username!);
            }
          }}
        />
      </>
    </Overlay>
  );
};

const mapStateToProps = (state: GlobalState) => ({
  userSession: state.userSession,
  transaction: state.transaction
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      topUp: balanceActionCreators.topUp,
      pay: balanceActionCreators.pay,
      endTransaction: transactionActionCreators.end
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PayAndTopUpOverlay);

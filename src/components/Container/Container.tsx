import * as React from "react";
import { StyleSheet, View, StyleProp, ViewStyle, Platform } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1,
    display: "flex",
    flexDirection: "column"
  },
  centered: {
    justifyContent: "center",
    alignItems: "center"
  },
  between: {
    justifyContent: "space-between"
  },
  noSafe: {
    flex: 1,
    paddingTop: Platform.OS === "ios" ? 10 : 0
  },
  full: {
    marginHorizontal: 0,
    paddingTop: 0
  }
});

interface ContainerProps {
  centered?: boolean;
  between?: boolean;
  keyboardAware?: boolean;
  noSafe?: boolean;
  full?: boolean;
  style?: StyleProp<ViewStyle>;
}

const withKeyboardAware = (Component: React.ReactNode) => (
  <KeyboardAwareScrollView
    keyboardShouldPersistTaps="handled"
    keyboardOpeningTime={0}
    enableOnAndroid
    extraScrollHeight={0}
  >
    {Component}
  </KeyboardAwareScrollView>
);

const Container: React.FunctionComponent<ContainerProps> = ({
  children,
  centered,
  between,
  keyboardAware,
  noSafe,
  full,
  style
}) => {
  const ContentComponent = noSafe ? (
    <View
      style={[
        styles.container,
        centered && styles.centered,
        between && styles.between,
        styles.noSafe,
        full && styles.full,
        style
      ]}
    >
      {children}
    </View>
  ) : (
    <SafeAreaView
      style={[
        styles.container,
        centered && styles.centered,
        between && styles.between,
        full && styles.full,
        style
      ]}
    >
      {children}
    </SafeAreaView>
  );

  return keyboardAware ? withKeyboardAware(ContentComponent) : ContentComponent;
};

export default Container;

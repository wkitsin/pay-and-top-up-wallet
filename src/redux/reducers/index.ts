import { combineReducers } from "redux";
import balance from "./balance";
import userSession from "./userSession";
import transaction from "./transaction";

export const rootReducer = combineReducers({
  userSession,
  balance,
  transaction
});

export type userSessionState = ReturnType<typeof rootReducer>;

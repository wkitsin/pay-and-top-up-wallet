import {
  transactionActions,
  transactionActionTypes
} from "../../actions/transaction";

export interface TransactionState {
  startTransaction: boolean;
}

const InitialState: TransactionState = {
  startTransaction: false
};

type transactionAction =
  | transactionActionTypes.end
  | transactionActionTypes.start;

const transaction = (state = InitialState, action: transactionAction) => {
  switch (action.type) {
    case transactionActions.START_TRANSACTION:
      return Object.assign({}, state, {
        startTransaction: true
      });

    case transactionActions.END_TRANSACTION:
      return Object.assign({}, state, {
        startTransaction: false
      });

    default:
      return state;
  }
};

export default transaction;

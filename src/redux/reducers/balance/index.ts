import { balanceActions, balanceActionTypes } from "../../actions/balance";
import { users } from "../../../data/data";
import update from "immutability-helper";

export interface BalanceState {
  username: users;
  balance: number;
  owe: {
    username?: users;
    amount: number;
  };
}

const InitialState: BalanceState[] = [
  {
    username: users.alice,
    balance: 100,
    owe: { amount: 0 }
  },
  {
    username: users.bob,
    balance: 80,
    owe: { amount: 0 }
  }
];

type balanceAction = balanceActionTypes.topUp | balanceActionTypes.pay;

const balance = (state = InitialState, action: balanceAction) => {
  switch (action.type) {
    case balanceActions.TOP_UP:
      const topUpAmount = parseInt(action.payload.amount.toString());
      const userIndex = state.findIndex(
        selectedState => selectedState.username === action.payload.user
      );

      // When topping up without owing anything
      if (state[userIndex].owe.amount === 0) {
        return update(state, {
          [userIndex]: {
            balance: {
              $set: state[userIndex].balance + topUpAmount
            }
          }
        });
      } else if (state[userIndex].owe.amount > topUpAmount) {
        // when topping up an amount lesser than the owe amount

        const newOweAmount = state[userIndex].owe.amount - topUpAmount;
        const ower = state.findIndex(
          selectedState =>
            selectedState.username === state[userIndex].owe.username
        );

        return update(state, {
          [userIndex]: {
            owe: {
              amount: { $set: newOweAmount }
            }
          },
          [ower]: {
            balance: {
              $set: topUpAmount + state[ower].balance
            }
          }
        });
      } else if (state[userIndex].owe.amount <= topUpAmount) {
        // when topping up an amount more or equal than the owe amount

        const newBalance = topUpAmount - state[userIndex].owe.amount;
        const ower = state.findIndex(
          selectedState =>
            selectedState.username === state[userIndex].owe.username
        );

        return update(state, {
          [userIndex]: {
            owe: {
              username: { $set: undefined },
              amount: { $set: 0 }
            },
            balance: { $set: newBalance }
          },
          [ower]: {
            balance: {
              $set: state[ower].balance + state[userIndex].owe.amount
            }
          }
        });
      } else {
        return state;
      }

    case balanceActions.PAY:
      const payAmount = parseInt(action.payload.amount.toString());
      const recipientIndex = state.findIndex(
        selectedState => selectedState.username === action.payload.receiver
      );
      const senderIndex = state.findIndex(
        selectedState => selectedState.username === action.payload.sender
      );

      // When paying an amount lesser than the balance
      if (state[senderIndex].balance > payAmount) {
        // When paying to someone who does not owes you
        if (state[recipientIndex].owe.amount === 0) {
          return update(state, {
            [senderIndex]: {
              balance: {
                $set: state[senderIndex].balance - payAmount
              }
            },
            [recipientIndex]: {
              balance: {
                $set: state[recipientIndex].balance + payAmount
              }
            }
          });
        } else if (state[recipientIndex].owe.amount < payAmount) {
          // When paying more than the recipient owe amount
          return update(state, {
            [recipientIndex]: {
              owe: {
                amount: { $set: 0 },
                $unset: ["username"]
              },
              balance: { $set: payAmount - state[recipientIndex].owe.amount }
            },
            [senderIndex]: {
              balance: {
                $set: state[senderIndex].balance - payAmount
              }
            }
          });
        } else {
          return update(state, {
            [recipientIndex]: {
              owe: {
                amount: { $set: state[recipientIndex].owe.amount - payAmount }
              }
            }
          });
        }
      } else {
        const oweAmount = payAmount - state[senderIndex].balance;

        return update(state, {
          [senderIndex]: {
            balance: { $set: 0 },
            owe: {
              username: { $set: state[recipientIndex].username },
              amount: { $set: state[senderIndex].owe.amount + oweAmount }
            }
          },
          [recipientIndex]: {
            balance: {
              $set: state[recipientIndex].balance + state[senderIndex].balance
            }
          }
        });
      }

    default:
      return state;
  }
};

export default balance;

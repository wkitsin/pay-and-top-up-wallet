import { BalanceState } from "./balance";
import { UserSessionState } from "./userSession";
import { TransactionState } from "./transaction";

export interface GlobalState {
  userSession: UserSessionState;
  balance: BalanceState[];
  transaction: TransactionState;
}

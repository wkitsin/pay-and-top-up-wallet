import { users } from "../../../data/data";
import {
  userSessionActions,
  userSessionActionTypes
} from "../../actions/userSession";

export interface UserSessionState {
  username: undefined | users;
  isLoading: boolean;
}

const InitialState: UserSessionState = {
  username: undefined,
  isLoading: true
};

type userSessionAction =
  | userSessionActionTypes.request
  | userSessionActionTypes.remove
  | userSessionActionTypes.set;

const userSession = (state = InitialState, action: userSessionAction) => {
  switch (action.type) {
    case userSessionActions.SET_USER_SESSION:
      return Object.assign({}, state, {
        isLoading: false,
        username: action.payload.username
      });

    case userSessionActions.REMOVE_USER_SESSION:
      return Object.assign({}, state, {
        isLoading: false,
        username: undefined
      });

    default:
      return state;
  }
};

export default userSession;

import { all, take, fork, put, cancel } from "redux-saga/effects";
import { balanceActions } from "../../actions/balance";
import {
  transactionActionCreators,
  transactionActions
} from "../../actions/transaction";

function* topUp() {
  while (true) {
    yield take(balanceActions.TOP_UP);
    yield put(transactionActionCreators.end());
  }
}

function* pay() {
  while (true) {
    yield take(balanceActions.PAY);
    yield put(transactionActionCreators.end());
  }
}

function* watchTransaction() {
  while (true) {
    yield take(transactionActions.START_TRANSACTION);

    // These task should run with no blocking
    const topUpFork = yield fork(topUp);
    const payFork = yield fork(pay);

    const endTransaction = yield take(transactionActions.END_TRANSACTION);

    // cancel topUp and pay saga if we close the transaction
    if (endTransaction) {
      yield cancel([topUpFork, payFork]);
    }
  }
}

export default function* index() {
  yield all([watchTransaction()]);
}

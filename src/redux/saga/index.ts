import { all } from "redux-saga/effects";
import userSessionSaga from "../saga/userSession";
import balanceSaga from "../saga/balance";

export default function* rootSaga() {
  yield all([userSessionSaga(), balanceSaga()]);
}

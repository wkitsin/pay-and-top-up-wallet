import AsyncStorage from "@react-native-async-storage/async-storage";
import { put, call, take, all } from "redux-saga/effects";
import {
  userSessionActionCreators,
  userSessionActions
} from "../../actions/userSession";
import { users } from "../../../data/data";

const getToken = async () => await AsyncStorage.getItem("userSession");

const setToken = async (token: string) => {
  return await AsyncStorage.setItem("userSession", token);
};

const removeToken = async () => await AsyncStorage.removeItem("userSession");

const authorize = (username: string) => {
  if (username === users.alice || username === users.bob) {
    return username;
  } else {
    throw new Error("wrong username");
  }
};

function* restoreUserSession() {
  while (true) {
    yield take(userSessionActions.REQUEST_USER_SESSION);
    const data = yield call(getToken);

    if (data) {
      yield put(userSessionActionCreators.setUserSession(data));

      yield take(userSessionActions.REMOVE_USER_SESSION);
      yield call(removeToken);
    } else {
      yield put(userSessionActionCreators.removeUserSession());
    }
  }
}

function* watchLoginSession() {
  while (true) {
    const data = yield take(userSessionActions.START_LOGIN_USER_SESSION);

    try {
      const username = yield call(authorize, data.payload.username);
      yield call(setToken, username);
      yield put(userSessionActionCreators.setUserSession(username));

      yield take(userSessionActions.REMOVE_USER_SESSION);
      yield call(removeToken);
    } catch (e) {
      // TODO: handle error better
      console.log({ e });
    }
  }
}

export default function* index() {
  yield all([restoreUserSession(), watchLoginSession()]);
}

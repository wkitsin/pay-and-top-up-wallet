class UserSessionActions {
  public readonly REQUEST_USER_SESSION = "REQUEST_USER_SESSION";
  public readonly REMOVE_USER_SESSION = "REMOVE_USER_SESSION";
  public readonly SET_USER_SESSION = "SET_USER_SESSION";
  public readonly START_LOGIN_USER_SESSION = "START_LOGIN_USER_SESSION";
}

export declare namespace userSessionActionTypes {
  export type request = TypedActionEmpty<
    typeof userSessionActions.REQUEST_USER_SESSION
  >;
  export type remove = TypedActionEmpty<
    typeof userSessionActions.REMOVE_USER_SESSION
  >;
  export type set = TypedAction<
    typeof userSessionActions.SET_USER_SESSION,
    {
      username: string;
    }
  >;
  export type startLogin = TypedAction<
    typeof userSessionActions.START_LOGIN_USER_SESSION,
    {
      username: string;
    }
  >;
}

class UserSessionActionCreators {
  public requestUserSession = (): userSessionActionTypes.request => ({
    type: userSessionActions.REQUEST_USER_SESSION
  });

  public startLoginUserSession = (
    username: string
  ): userSessionActionTypes.startLogin => ({
    type: userSessionActions.START_LOGIN_USER_SESSION,
    payload: {
      username
    }
  });

  public setUserSession = (username: string): userSessionActionTypes.set => ({
    type: userSessionActions.SET_USER_SESSION,
    payload: {
      username
    }
  });

  public removeUserSession = (): userSessionActionTypes.remove => ({
    type: userSessionActions.REMOVE_USER_SESSION
  });
}

export const userSessionActions = new UserSessionActions();
export const userSessionActionCreators = new UserSessionActionCreators();

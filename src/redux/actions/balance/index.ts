import { users } from "../../../data/data";

class BalanceActions {
  public readonly TOP_UP = "TOP_UP";
  public readonly PAY = "PAY";
}

export declare namespace balanceActionTypes {
  export type topUp = TypedAction<
    typeof balanceActions.TOP_UP,
    {
      amount: number;
      user: users.bob | users.alice;
    }
  >;
  export type pay = TypedAction<
    typeof balanceActions.PAY,
    {
      amount: number;
      receiver: users.bob | users.alice;
      sender: users.alice | users.bob;
    }
  >;
}

class BalanceActionCreators {
  public pay = (
    amount: number,
    receiver: users.bob | users.alice,
    sender: users.bob | users.alice
  ): balanceActionTypes.pay => ({
    type: balanceActions.PAY,
    payload: {
      amount,
      receiver,
      sender
    }
  });

  public topUp = (
    amount: number,
    user: users.bob | users.alice
  ): balanceActionTypes.topUp => ({
    type: balanceActions.TOP_UP,
    payload: {
      amount,
      user
    }
  });
}

export const balanceActions = new BalanceActions();
export const balanceActionCreators = new BalanceActionCreators();

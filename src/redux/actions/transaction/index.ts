class TransactionAction {
  public readonly START_TRANSACTION = "START_TRANSACTION";
  public readonly END_TRANSACTION = "END_TRANSACTION";
}

export declare namespace transactionActionTypes {
  export type start = TypedAction<typeof transactionActions.START_TRANSACTION>;

  export type end = TypedAction<typeof transactionActions.END_TRANSACTION>;
}

class TransactionActionCreators {
  public start = (): transactionActionTypes.start => ({
    type: transactionActions.START_TRANSACTION
  });

  public end = (): transactionActionTypes.end => ({
    type: transactionActions.END_TRANSACTION
  });
}

export const transactionActions = new TransactionAction();
export const transactionActionCreators = new TransactionActionCreators();

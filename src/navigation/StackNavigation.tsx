import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { userSessionActionCreators } from "../redux/actions/userSession";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { GlobalState } from "../redux/reducers/type";
import { ActivityIndicator } from "react-native";
import HomeNavigation from "./HomeNavigation";
import AuthenticationNavigation from "./AuthenticationNavigation";
import Container from "../components/Container/Container";

interface StackNavigationStateProps {
  userSession: string;
  isLoading: boolean;
}

interface StackNavigationDispatchProps {
  requestUserSession: typeof userSessionActionCreators.requestUserSession;
}

type LandingProps = StackNavigationStateProps & StackNavigationDispatchProps;

const StackNavigation: React.FC<LandingProps> = ({
  requestUserSession,
  userSession,
  isLoading
}) => {
  React.useEffect(() => {
    requestUserSession();
  }, []);

  if (isLoading) {
    return (
      <Container centered>
        <ActivityIndicator />
      </Container>
    );
  }

  return (
    <NavigationContainer>
      {userSession ? HomeNavigation() : AuthenticationNavigation()}
    </NavigationContainer>
  );
};

const mapStateToProps = (state: GlobalState) => ({
  userSession: state.userSession.username,
  isLoading: state.userSession.isLoading
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { requestUserSession: userSessionActionCreators.requestUserSession },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(StackNavigation);

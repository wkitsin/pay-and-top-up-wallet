import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { HomeParamsList } from "../screens/Home/HomeParamsList";
import Home from "../screens/Home/Home/Home";

const Stack = createStackNavigator<HomeParamsList>();

const HomeNavigation = () => (
  <Stack.Navigator screenOptions={{ headerBackTitle: " " }}>
    <Stack.Screen
      name="Home"
      component={Home}
      options={{ headerTitle: "Wallet" }}
    />
  </Stack.Navigator>
);

export default HomeNavigation;

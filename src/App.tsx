/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import "react-native-gesture-handler";

import React from "react";
import { Provider } from "react-redux";
import store from "./redux/store/store";
import StackNavigation from "./navigation/StackNavigation";

const App = () => {
  return (
    <Provider store={store}>
      <StackNavigation />
    </Provider>
  );
};

export default App;

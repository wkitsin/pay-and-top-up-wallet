import { users } from "../../../src/data/data";
import { balanceActionCreators } from "../../../src/redux/actions/balance";
import balance from "../../../src/redux/reducers/balance";

const currentUser = users.alice;
const payTo = users.bob;

describe("top up reducer", () => {
  describe("top up with no owing", () => {
    it("should work", () => {
      const initialState = [
        {
          username: currentUser,
          balance: 100,
          owe: { amount: 0 }
        },
        {
          username: payTo,
          balance: 80,
          owe: { amount: 0 }
        }
      ];

      expect(
        balance(initialState, balanceActionCreators.topUp(20, currentUser))
      ).toEqual([
        {
          username: currentUser,
          balance: 120,
          owe: { amount: 0 }
        },
        {
          username: payTo,
          balance: 80,
          owe: { amount: 0 }
        }
      ]);
    });
  });

  describe("top up the exact owe amount", () => {
    it("will reduce the owe amount to 0 and increase the ower balance", () => {
      const initialState = [
        {
          username: currentUser,
          balance: 0,
          owe: { amount: 20, username: payTo }
        },
        {
          username: payTo,
          balance: 80,
          owe: { amount: 0 }
        }
      ];

      expect(
        balance(initialState, balanceActionCreators.topUp(20, currentUser))
      ).toEqual([
        {
          username: currentUser,
          balance: 0,
          owe: { amount: 0 }
        },
        {
          username: payTo,
          balance: 100,
          owe: { amount: 0 }
        }
      ]);
    });
  });

  describe("top up more than the owe amount", () => {
    it("will reduce the owe amount to 0 and increase the ower balance", () => {
      const initialState = [
        {
          username: currentUser,
          balance: 0,
          owe: { amount: 20, username: payTo }
        },
        {
          username: payTo,
          balance: 80,
          owe: { amount: 0 }
        }
      ];

      expect(
        balance(initialState, balanceActionCreators.topUp(80, currentUser))
      ).toEqual([
        {
          username: currentUser,
          balance: 60,
          owe: { amount: 0 }
        },
        {
          username: payTo,
          balance: 100,
          owe: { amount: 0 }
        }
      ]);
    });
  });

  describe("top up lesser than the owe amount", () => {
    it("will reduce the owe amount and increase the ower balance", () => {
      const initialState = [
        {
          username: currentUser,
          balance: 0,
          owe: { amount: 20, username: payTo }
        },
        {
          username: payTo,
          balance: 80,
          owe: { amount: 0 }
        }
      ];

      expect(
        balance(initialState, balanceActionCreators.topUp(10, currentUser))
      ).toEqual([
        {
          username: currentUser,
          balance: 0,
          owe: { amount: 10, username: payTo }
        },
        {
          username: payTo,
          balance: 90,
          owe: { amount: 0 }
        }
      ]);
    });
  });
});

describe("pay reducer", () => {
  describe("pay with no owing", () => {
    it("should work", () => {
      const initialState = [
        {
          username: currentUser,
          balance: 100,
          owe: { amount: 0 }
        },
        {
          username: payTo,
          balance: 80,
          owe: { amount: 0 }
        }
      ];

      expect(
        balance(initialState, balanceActionCreators.pay(20, payTo, currentUser))
      ).toEqual([
        {
          username: currentUser,
          balance: 80,
          owe: { amount: 0 }
        },
        {
          username: payTo,
          balance: 100,
          owe: { amount: 0 }
        }
      ]);
    });
  });

  describe("pay with an owe amount and no balance", () => {
    it("should increase the owe amount and not change the balance of the ower", () => {
      const initialState = [
        {
          username: currentUser,
          balance: 0,
          owe: { amount: 20, username: payTo }
        },
        {
          username: payTo,
          balance: 80,
          owe: { amount: 0 }
        }
      ];

      expect(
        balance(initialState, balanceActionCreators.pay(30, payTo, currentUser))
      ).toEqual([
        {
          username: currentUser,
          balance: 0,
          owe: { amount: 50, username: payTo }
        },
        {
          username: payTo,
          balance: 80,
          owe: { amount: 0 }
        }
      ]);
    });
  });

  describe("paying more than the balance", () => {
    it("should increase the owe amount and set the balance to 0", () => {
      const initialState = [
        {
          username: currentUser,
          balance: 50,
          owe: { amount: 0 }
        },
        {
          username: payTo,
          balance: 80,
          owe: { amount: 0 }
        }
      ];

      expect(
        balance(initialState, balanceActionCreators.pay(80, payTo, currentUser))
      ).toEqual([
        {
          username: currentUser,
          balance: 0,
          owe: { amount: 30, username: payTo }
        },
        {
          username: payTo,
          balance: 130,
          owe: { amount: 0 }
        }
      ]);
    });
  });

  describe("paying someone who owes you", () => {
    it("should decrease their owe amount", () => {
      const initialState = [
        {
          username: currentUser,
          balance: 0,
          owe: { amount: 50, username: payTo }
        },
        {
          username: payTo,
          balance: 80,
          owe: { amount: 0 }
        }
      ];

      expect(
        balance(initialState, balanceActionCreators.pay(20, currentUser, payTo))
      ).toEqual([
        {
          username: currentUser,
          balance: 0,
          owe: { amount: 30, username: payTo }
        },
        {
          username: payTo,
          balance: 80,
          owe: { amount: 0 }
        }
      ]);
    });
  });

  describe("paying an amount more than the owing amount", () => {
    it("should set their owe amount to zero and increase the balace", () => {
      const initialState = [
        {
          username: currentUser,
          balance: 0,
          owe: { amount: 20, username: payTo }
        },
        {
          username: payTo,
          balance: 80,
          owe: { amount: 0 }
        }
      ];

      expect(
        balance(initialState, balanceActionCreators.pay(50, currentUser, payTo))
      ).toEqual([
        {
          username: currentUser,
          balance: 30,
          owe: { amount: 0 }
        },
        {
          username: payTo,
          balance: 30,
          owe: { amount: 0 }
        }
      ]);
    });
  });
});

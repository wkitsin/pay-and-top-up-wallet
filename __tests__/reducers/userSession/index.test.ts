import { users } from "../../../src/data/data";
import { userSessionActionCreators } from "../../../src/redux/actions/userSession";
import userSession from "../../../src/redux/reducers/userSession";

describe("user session reducer", () => {
  describe("removing the user session", () => {
    it("should return ", () => {
      const initialState = {
        username: undefined,
        isLoading: true
      };

      expect(
        userSession(initialState, userSessionActionCreators.removeUserSession())
      ).toEqual({
        username: undefined,
        isLoading: false
      });
    });
  });

  describe("setting the user session", () => {
    const initialState = {
      username: undefined,
      isLoading: true
    };

    expect(
      userSession(
        initialState,
        userSessionActionCreators.setUserSession(users.alice)
      )
    ).toEqual({
      username: users.alice,
      isLoading: false
    });
  });
});

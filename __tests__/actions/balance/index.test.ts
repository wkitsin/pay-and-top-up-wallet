import { users } from "../../../src/data/data";
import {
  balanceActionCreators,
  balanceActions
} from "../../../src/redux/actions/balance";

describe("actions", () => {
  it("should create an action to pay", () => {
    const expectedAction = {
      type: balanceActions.PAY,
      payload: {
        amount: 10,
        receiver: users.alice,
        sender: users.bob
      }
    };

    expect(balanceActionCreators.pay(10, users.alice, users.bob)).toEqual(
      expectedAction
    );
  });

  it("should create an action to start the login", () => {
    const expectedAction = {
      type: balanceActions.TOP_UP,
      payload: { amount: 20, user: users.bob }
    };

    expect(balanceActionCreators.topUp(20, users.bob)).toEqual(expectedAction);
  });
});

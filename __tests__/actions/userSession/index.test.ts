import { users } from "../../../src/data/data";
import {
  userSessionActionCreators,
  userSessionActions
} from "../../../src/redux/actions/userSession";

describe("actions", () => {
  it("should create an action to restore the user session", () => {
    const expectedAction = {
      type: userSessionActions.REQUEST_USER_SESSION
    };

    expect(userSessionActionCreators.requestUserSession()).toEqual(
      expectedAction
    );
  });

  it("should create an action to start the login", () => {
    const username = users.alice;
    const expectedAction = {
      type: userSessionActions.START_LOGIN_USER_SESSION,
      payload: { username }
    };

    expect(userSessionActionCreators.startLoginUserSession(username)).toEqual(
      expectedAction
    );
  });

  it("should create an action to set the user sesseion", () => {
    const username = users.alice;
    const expectedAction = {
      type: userSessionActions.SET_USER_SESSION,
      payload: { username }
    };

    expect(userSessionActionCreators.setUserSession(username)).toEqual(
      expectedAction
    );
  });

  it("should create an action to remove the user sesseion", () => {
    const expectedAction = {
      type: userSessionActions.REMOVE_USER_SESSION
    };

    expect(userSessionActionCreators.removeUserSession()).toEqual(
      expectedAction
    );
  });
});
